%%%%%%%%%%% In the name of GOD %%%%%%%%%%%
% Course: DSP BSC course - Dr. Sheikhzadeh
% demo program by amir hossein rassafi in March,2018
clc 
clear
Fs = 200;
f0 = 45;
f1 = 30;
N = 1024;

n = 0:1/Fs:(N-1)/f0;
x1 = cos(2*pi*f0*n)+10*cos(2*pi*f1*n);
%%
%expand
M = [0 1 2] %number of zero that will be add between to sample
for z = M
    x = zeros(1,(z+1)*length(x1));
    x(1:z+1:end)=x1;
    figure
    freqz(x,[1],4096,Fs)
    title(sprintf("expand with %d zero",z))
end
%%
%compress
K = [0 1 2] %number of sample that will be add removed 
for s = K
    x = zeros(1,cast(length(x1)/(s+1),'uint8'));
    x = x1(1:s+1:end);
    figure
    freqz(x,[1],4096,Fs)
    title(sprintf("compressed with %d sample",s)) 
end
%%
%1-compress with 1
%2-expand with 1
figure 
freqz(x1,[1],4096,Fs)
o = 1;
x = zeros(1,cast(length(x1)/(o+1),'uint8'));
x = x1(1:o+1:end);
figure
freqz(x,[1],4096,Fs)
X = zeros(1,(o+1)*length(x));
X(1:o+1:end)=x;
figure
freqz(X,[1],4096,Fs)
%%
%1-expand with 1
%2-compress with 1
figure 
freqz(x1,[1],4096,Fs)
o = 1;
x = zeros(1,(o+1)*length(x1));
x(1:o+1:end)=x1;
figure
freqz(x,[1],4096,Fs)
X = zeros(1,cast(length(x)/(o+1),'uint8'));
X = x(1:o+1:end);
figure
freqz(X,[1],4096,Fs)
