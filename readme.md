# _MATLAB TA session 3_

## "sampling.m" file

### _sampling a sin(2pi*100*t)_

![image](/uploads/9b000b0d16fdbd7cab85e959db045b3a/image.png)
![image](/uploads/54b86b7e2689fb83199b9c7a6e313aa5/image.png)

## "expcom.m" file

![image](/uploads/01a7ba2311e1a1a69bbfcd180714f853/image.png)
![image](/uploads/bd8758370ce88d27abec99d411afcdc2/image.png)

#### _Please check the files carefully_
