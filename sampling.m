%%%%%%%%%%% In the name of GOD %%%%%%%%%%%
% Course: DSP BSC course - Dr. Sheikhzadeh
% demo program by amir hossein rassafi in March,2018
%f_new = f_old + k*fs
clc 
clear
f0 = 100;
fs = 1000e3;
ts = linspace(-5,5,100);
nC = 20;
Fs = [70 150 200 300];
for f = Fs
    t =0:1/f:nC/f0;
    s = sin(2*pi*f0*t);
    T=0:1/fs:nC/f0;
    X = sin(2*pi*f0*T);
    figure
    plot(t,s,'ro',T,X,'b')
    title(sprintf("Fs = %d",f))
    figure
    freqz(s,[1],1024,f)
    title(sprintf("Fs = %d",f))
end
